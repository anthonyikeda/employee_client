require 'rest-client'

module EmployeeClient

  class Client

    def get_employee(employee_id)
      puts "getting employee #{employee_id}"
      @endpoint = EmployeeClient.configuration.base_url + "/employee/#{employee_id}"

      puts @endpoint

      response = RestClient.get @endpoint, :accept => :json
      JSON.parse(response.to_str)
    end

    def get_employees(parms = {})
      parms[:limit] ||= "10"
      parms[:offset] ||= "0"

      @endpoint = EmployeeClient.configuration.base_url + "/employee"

      response = RestClient.get @endpoint, {:params => {:offset => parms[:offset], :limit => parms[:limit]}}
      puts response
      JSON.parse(response.to_str)
    end

    def create_employee(employee)
    	@endpoint = EmployeeClient.configuration.base_url + "/employee"

    	response = RestClient.post @endpoint, employee.to_json, {:content_type => :json}

    	puts response.code
    	puts response.to_str
    	JSON.parse(response.to_str)

    end
  end

end
