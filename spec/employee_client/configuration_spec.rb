require 'spec_helper'

module EmployeeClient
  describe Configuration do

    describe "#base_url" do
      it "default value of 'http://192.168.99.100:9292" do
        Configuration.new.base_url = 'http://192.168.99.100:9292'
      end
    end

    describe "#base_url=" do
      it "should accept the default Configuration" do 
        config = Configuration.new
        config.base_url = "http://192.168.99.100:9292"
        expect(config.base_url).to eq("http://192.168.99.100:9292")
      end
    end
  end
end

