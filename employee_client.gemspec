Gem::Specification.new do |s|
  s.name          = 'employee_client'
  s.version       = '1.0'
  s.date          = '2015-11-23'
  s.summary       = "Client to call the employee api"
  s.description   = "Uses RestClient to call the endpoints in the Employee API"
  s.authors       = ["Anthony Ikeda"]
  s.email         = "anthony.ikeda@gmail.com"
  s.files         = ["lib/employee_client.rb", "lib/employee_client/client.rb", "lib/employee_client/configuration.rb"]
  s.homepage      = 'http://locahost/gems'
  s.license       = 'Public Domain'

  s.test_files    = s.files.grep(%r{^(test|spec|features)/})
  s.require_paths = ["lib"]

  s.add_development_dependency "bundler", "~> 1.6"
  s.add_development_dependency "rake", "~> 10.4", ">= 10.4.2"
  s.add_development_dependency "rspec", "~> 3.4"
  s.add_development_dependency "rest-client", "~> 1.8"
end
