require 'spec_helper'
require 'employee_client'

module EmployeeClient

  describe Client do
    describe "#configure" do

      before do
        EmployeeClient.configure do |config|
          # config.base_url = "http://192.168.99.100:9292"
          config.base_url = "http://localhost:9292"
        end
      end

      it "returns a single employee" do
        employee = Client.new.get_employee(1)
        puts employee
        expect(employee["employee_id"]).to eq(1)
        expect(employee["first_name"]).to eq("Anthony")
        expect(employee["last_name"]).to eq("Ikeda")
      end

      it "returns a list of employees 10 or less" do
        parms = {}
        parms[:limit] = 2

        employees = Client.new.get_employees parms
        puts employees
        expect(employees).not_to  be_empty
        expect(employees.size).to be <= 10

        employees.each do |emp|
          puts "Employee Id: #{emp["employee_id"]} first_name: #{emp["first_name"]}"
        end
      end

      it "creates a new employee" do
        employee = {}
        employee[:first_name] = "Paul"
        employee[:last_name] = "Johnson"

        createdEmp = Client.new.create_employee employee

        expect(createdEmp).not_to be_nil
        expect(createdEmp["employee_id"]).to be > 1
      end

      after :each do
        EmployeeClient.reset
      end
    end

    describe ".reset" do
      before :each do
        EmployeeClient.configure do |config|
          config.base_url = "http://yahoo.com.au"
        end
      end

      it "resets the configuration" do
        EmployeeClient.reset

        config = EmployeeClient.configuration

        expect(config.base_url).to eq("http://192.168.99.100:9292")
      end
    end
  end
end
